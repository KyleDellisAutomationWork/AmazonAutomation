﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AmazonUITests
{
    using AmazonUI;

    [TestClass]
    public class TodaysDealsTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Driver.Initialize();
        }

        /// <summary>
        /// Starts at Home page and verifies user successfully navigated to the Today's Deals page
        /// </summary>
        [TestMethod]
        public void CanNavigateToPage()
        {
            HomePage.GoTo();
            NavMenu.TodaysDeals();

            Assert.IsTrue(TodaysDealsPage.IsAt, "Failed to navigate to Today's Deals page");
        }

        /// <summary>
        /// Navigates from the Home page to Today's Deals page. From there clicks the sort dropdown and sort by Price - Low to High.
        /// Verifies the dropdown is displaying the correct option and that each item is in the correct sort order
        /// </summary>
        [TestMethod]
        public void CanSortByPriceLowToHigh()
        {
            HomePage.GoTo();
            NavMenu.TodaysDeals();
            TodaysDealsPage.SortByPriceLowToHigh();
            
            Assert.IsTrue(TodaysDealsPage.SortByPriceLowToHighDisplayed, "'Sort by' dropdown does not display 'Price - Low to High'");
            Assert.IsTrue(TodaysDealsPage.ProductPricesSortedLowToHigh, "Products on page are not sorted by low price to high price");
        }

        /// <summary>
        /// Navigates from the Home page to Today's Deals page. From there clicks the sort dropdown and sort by Price - Low to High.
        /// Then verifies that every product has an image, a name, a price, and that the price is in the correct format.
        /// </summary>
        [TestMethod]
        public void CanProperlySeeSortedProductsAttributes()
        {
            HomePage.GoTo();
            NavMenu.TodaysDeals();
            TodaysDealsPage.SortByPriceLowToHigh();

            Assert.IsTrue(TodaysDealsPage.ProductsContainImage, "One or more products was missing an image");
            Assert.IsTrue(TodaysDealsPage.ProductsContainName, "One or more products was missing a name");
            Assert.IsTrue(TodaysDealsPage.ProductsContainPrice, "One or more products was missing a price");
            Assert.IsTrue(TodaysDealsPage.ProductsPriceFormatted, "One or more products had an incorreclty formatted price");
        }

        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
    }
}

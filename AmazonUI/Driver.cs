﻿namespace AmazonUI
{
    using System;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;

    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            Instance = new FirefoxDriver();
        }

        public static void Close()
        {
            Instance.Close();
        }
    }
}
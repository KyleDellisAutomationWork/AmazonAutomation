﻿namespace AmazonUI
{
    public class HomePage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl("http://amazon.com");
        }
    }
}

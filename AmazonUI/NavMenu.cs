﻿namespace AmazonUI
{
    using System;
    using System.Threading;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.UI;

    public class NavMenu
    {
        public static void TodaysDeals()
        {
            var todaysDealsButton = Driver.Instance.FindElement(By.LinkText("Today's Deals"));
            todaysDealsButton.Click();
            Thread.Sleep(4000);
        }
    }
}

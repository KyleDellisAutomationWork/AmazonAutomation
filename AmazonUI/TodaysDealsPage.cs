﻿namespace AmazonUI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using OpenQA.Selenium;

    public class TodaysDealsPage
    {
        public static bool IsAt
        {
            get
            {
                var pageHeader = Driver.Instance.FindElement(By.ClassName("gbh1-bold"));
                if (pageHeader.Text == "Today's Deals")
                {
                    return true;
                }
                return false;
            }
        }

        public static bool SortByPriceLowToHighDisplayed
        {
            get
            {
                var sortDropdown = Driver.Instance.FindElement(By.ClassName("a-dropdown-prompt"));
                if (sortDropdown.Text == "Price - Low to High")
                {
                    return true;
                }
                return false;
            }
        }

        public static bool ProductPricesSortedLowToHigh
        {
            get
            {
                var products = Driver.Instance.FindElements(By.XPath("//*[starts-with(@id, '100_dealView_')]//div[contains(@class, 'priceBlock')]/span"));
                var previousProduct = 0.00;
                var currentProduct = 0.00;
                int index;
                foreach (var product in products)
                {
                    if (product.Text.Contains("-"))
                    {
                        index = product.Text.IndexOf(" ");
                    }
                    else
                    {
                        index = product.Text.Length - 1;
                    }
                    var substring = product.Text.Substring(1, index);
                    currentProduct = Convert.ToDouble(substring);
                    if (currentProduct < previousProduct)
                    {
                        return false;
                    }
                    previousProduct = currentProduct;
                }
                return true;
            }
        }

        public static bool ProductsContainImage
        {
            get
            {
                var productImages = Driver.Instance.FindElements(By.XPath("//*[starts-with(@id, '100_dealView_')]//img"));
                foreach (var productImage in productImages)
                {
                    if (productImage == null || productImage.Displayed == false)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public static bool ProductsContainName
        {
            get
            {
                var productNames = Driver.Instance.FindElements(By.XPath("//*[starts-with(@id, '100_dealView_')]//span[@id='dealTitle'][2]"));
                foreach (var productName in productNames)
                {
                    if (productName == null || productName.Text == "" || productName.Displayed == false)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public static bool ProductsContainPrice
        {
            get
            {
                var productPrices = Driver.Instance.FindElements(By.XPath("//*[starts-with(@id, '100_dealView_')]//div[contains(@class, 'priceBlock')]/span"));
                foreach (var productPrice in productPrices)
                {
                    if (productPrice == null || productPrice.Text == "" || productPrice.Displayed == false)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public static bool ProductsPriceFormatted
        {
            get
            {
                var productPrices = Driver.Instance.FindElements(By.XPath("//*[starts-with(@id, '100_dealView_')]//div[contains(@class, 'priceBlock')]/span"));
                NumberStyles style;
                style = NumberStyles.AllowCurrencySymbol | NumberStyles.AllowDecimalPoint;
                foreach (var productPrice in productPrices)
                {
                    if (productPrice.Text.Contains("-"))
                    {
                        string[] delimiterString = new string[] { " - " };
                        string[] splitStrings = productPrice.Text.Split(delimiterString, StringSplitOptions.None);
                        foreach (var splitString in splitStrings)
                        {
                            if (Decimal.TryParse(splitString, style, CultureInfo.CurrentCulture, out decimal result) == false)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        if (Decimal.TryParse(productPrice.Text, style, CultureInfo.CurrentCulture, out decimal result) == false)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public static void SortByPriceLowToHigh()
        {
            var sortDropdown = Driver.Instance.FindElement(By.Name("sortOptions"));
            sortDropdown.Click();
            Thread.Sleep(4000);

            var sortDropdownLowToHighOption = Driver.Instance.FindElement(By.LinkText("Price - Low to High"));
            sortDropdownLowToHighOption.Click();
            Thread.Sleep(4000);
        }
    }
}
